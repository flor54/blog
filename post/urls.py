from django.conf.urls import url
from django.contrib import admin

from .views import (
	post_list,
	post_create,
	PostDetailView,
	post_delete,
	post_update
	)


app_name = "posts"

urlpatterns = [
	url(r'^$', post_list, name='list'),
	url(r'^create/$', post_create),
	url(r'^(?P<slug>[\w-]+)/$', PostDetailView.as_view(), name='detail'),
	url(r'^(?P<slug>[\w-]+)/edit/$', post_update, name='update'),
	url(r'^(?P<slug>[\w-]+)/delete/$', post_delete),
]